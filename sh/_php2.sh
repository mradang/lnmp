#!/bin/bash

yum install -y --nogpgcheck https://mirrors.tuna.tsinghua.edu.cn/remi/enterprise/remi-release-$(rpm -E %centos).rpm

find /etc/yum.repos.d/ -name "remi*.repo" | xargs sed -i "s/#baseurl/baseurl/g"
find /etc/yum.repos.d/ -name "remi*.repo" | xargs sed -i "s/mirrorlist/#mirrorlist/g"
find /etc/yum.repos.d/ -name "remi*.repo" | xargs sed -i "s@http://rpms.remirepo.net@https://mirrors.tuna.tsinghua.edu.cn/remi@g"
yum makecache
yum install yum-utils -y
yum-config-manager --disable 'remi-php*'
yum-config-manager --enable remi-php81
yum repolist
yum update
yum install -y php php-fpm
yum install -y php-mysql php-mbstring php-xml php-gd php-pecl-zip php-bcmath php-pecl-redis

systemctl enable php-fpm
systemctl start php-fpm
