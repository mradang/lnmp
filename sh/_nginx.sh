#!/bin/bash

yum install yum-utils -y

\cp $bashpath/nginx/nginx.repo /etc/yum.repos.d/

yum-config-manager --enable nginx-mainline

yum install -y nginx
cd /etc/nginx

cpucores=$(cat /proc/cpuinfo | grep "cpu cores" | uniq | awk '{print $4}')
sed -i "s/^worker_processes.*$/worker_processes $cpucores;/" nginx.conf

sed -i '/events {/a\    use epoll;' nginx.conf
sed -i '/http {/a\    server_tokens off;' nginx.conf

\cp $bashpath/nginx/*.config /etc/nginx/conf.d/

firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

systemctl enable nginx
systemctl start nginx

echo '0 3 * * * /usr/local/bin/files.backup.sh nginx 90 /etc/nginx' >>/var/spool/cron/root
