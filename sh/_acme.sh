#!/bin/bash

curl https://get.acme.sh | sh
source ~/.bashrc
acme.sh --set-default-ca --server letsencrypt
acme.sh --install-cronjob
