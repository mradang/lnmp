#!/bin/bash

yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y https://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum install -y yum-utils
yum-config-manager --disable 'remi-php*'
yum-config-manager --enable remi-php81
yum update
yum install -y php php-fpm
yum install -y php-mysql php-mbstring php-xml php-gd php-pecl-zip php-bcmath php-pecl-redis

systemctl enable php-fpm
systemctl start php-fpm
