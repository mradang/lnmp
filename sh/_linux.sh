#!/bin/bash

# 防火墙配置
systemctl enable firewalld
systemctl start firewalld

# 文件备份脚本
\cp $bashpath/linux/files.backup.sh /usr/local/bin/
chmod u+x /usr/local/bin/files.backup.sh

# 必备软件
yum install xorg-x11-xauth zip unzip lrzsz -y

# 系统更新
yum update -y
reboot
