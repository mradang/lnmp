#!/bin/bash

rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022

\cp $bashpath/mysql/mysql-community.repo /etc/yum.repos.d/
yum install mysql-server -y

systemctl enable mysqld
systemctl start mysqld

temp_password=$(grep 'temporary password' /var/log/mysqld.log | awk '{print $NF}')
echo "临时密码：$temp_password"

password=""
while [ ! $password ]
do
  read -p "请输入 root 密码：" password
done

mysql -uroot -p"$temp_password" --connect-expired-password -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '$password';"

\cp $bashpath/mysql/mysql.backup.sh /usr/local/bin/
chmod u+x /usr/local/bin/mysql.backup.sh

echo "[client]
user=root
password=\"$password\"
[mysqldump]
user=root
password=\"$password\"" > ~/.my.cnf
chmod 600 ~/.my.cnf

echo '0 3 * * * /usr/local/bin/mysql.backup.sh >> /var/log/mysql.backup.log' >> /var/spool/cron/root
