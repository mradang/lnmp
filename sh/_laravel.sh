#!/bin/bash

if [[ ! -f /usr/bin/supervisorctl ]]; then
  yum install -y supervisor
  systemctl enable supervisord
  systemctl start supervisord
fi

APP=""

while [[ ! $APP || ! -d /var/www/$APP ]]
do
  read -p "请输入 Laravel 项目在 /var/www 下的目录名：" APP
done

echo "[program:laravel-worker-${APP}]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/${APP}/artisan queue:work --sleep=3 --tries=3
user=apache
numprocs=8
" > /etc/supervisord.d/$APP.ini

/usr/bin/supervisorctl reload

# 写入定时任务
exists=$(cat /etc/crontab |grep /var/www/$APP/artisan|wc -l)
if [[ $exists -eq 0 ]]; then
  echo "* * * * * apache /usr/bin/php /var/www/$APP/artisan schedule:run" >> /etc/crontab
fi

# 备份配置文件
echo "0 3 * * * /usr/local/bin/files.backup.sh laravel-$APP 90 /var/www/$APP/.env" >> /var/spool/cron/root
