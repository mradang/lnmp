#!/bin/bash

hostname=""

while [ ! $hostname ]; do
  read -p "请输入主机域名：" hostname
done

DP_Id=""
while [ ! $DP_Id ]
do
  read -p "请输入 DP_Id：" DP_Id
done
export DP_Id="$DP_Id"

DP_Key=""
while [ ! $DP_Key ]
do
  read -p "请输入 DP_Key：" DP_Key
done
export DP_Key="$DP_Key"

/root/.acme.sh/acme.sh --issue --dns dns_dp -d "*.$hostname"

\cp $bashpath/site/ssl.hostname.config /etc/nginx/conf.d/ssl.$hostname.config
\cp $bashpath/site/status.hostname.sample /etc/nginx/conf.d/status.$hostname.conf
sed -i "s/{hostname}/$hostname/" /etc/nginx/conf.d/ssl.$hostname.config
sed -i "s/{hostname}/$hostname/" /etc/nginx/conf.d/status.$hostname.conf

mkdir -p /etc/nginx/ssl

/root/.acme.sh/acme.sh --install-cert -d "*.$hostname" --key-file /etc/nginx/ssl/$hostname.key --fullchain-file /etc/nginx/ssl/$hostname.crt --reloadcmd "systemctl force-reload nginx"
