#!/bin/bash

# 创建密码文件
mkdir -p /etc/rsyncd
password=$(openssl rand -base64 32 | tr -d /=+ | cut -c -16)
echo "backup:${password}" > /etc/rsyncd/server.pass
chmod 600 /etc/rsyncd/server.pass

# 修改 rsyncd 配置
exists=$(cat /etc/rsyncd.conf |grep /etc/rsyncd/server.pass|wc -l)
if [[ $exists -eq 0 ]]; then
  echo "secrets file = /etc/rsyncd/server.pass
read only = true
hosts allow = *
auth users = backup
list = no

[backup]
path = /backup" >> /etc/rsyncd.conf
fi

# 启动服务
systemctl start rsyncd
systemctl enable rsyncd

# SELinux
setsebool -P rsync_export_all_ro=1

# 显示密码文件
cat /etc/rsyncd/server.pass
