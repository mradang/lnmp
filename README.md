# LNMP 环境部署（CentOS 7）

## 安装必备软件

```
yum install git -y
```

## 获取配置脚本

```
git clone https://gitee.com/mradang/lnmp.git
```

## 运行配置脚本

```
cd lnmp
chmod u+x run.sh
./run.sh
```

## 更新

```
rm lnmp/ -rf; git clone git@gitee.com:mradang/lnmp.git; chmod u+x lnmp/run.sh;
```
