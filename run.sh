#!/bin/bash

flag=true
while $flag
do
  echo "LNMP 部署脚本：
1. 系统初始化
2. 安装 MySQL8
3. 安装 PHP8
4. 安装 Redis
5. 安装 Nginx
6. 安装 acme.sh
7. 配置 站点
8. 配置 Laravel 应用
9. 配置 Rsync"
  read -p "请选择任务：" channel
  expr $channel + 0 &>/dev/null
  [ $? -eq 0 ] && [ $channel -ge 1 ] && [ $channel -le 9 ] && flag=false
done

export bashpath=$(cd `dirname $0`;pwd)

case $channel in
  1)
    sh ./sh/_linux.sh
    ;;
  2)
    sh ./sh/_mysql.sh
    ;;
  3)
    sh ./sh/_php.sh
    ;;
  4)
    sh ./sh/_redis.sh
    ;;
  5)
    sh ./sh/_nginx.sh
    ;;
  6)
    sh ./sh/_acme.sh
    ;;
  7)
    sh ./sh/_host.sh
    ;;
  8)
    sh ./sh/_laravel.sh
    ;;
  9)
    sh ./sh/_rsync.sh
    ;;
esac

echo -e "\n"
exec $BASH_SOURCE
