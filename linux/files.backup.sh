#!/bin/bash

# 使用说明：files.backup.sh backupname days file1 dir1 file2 ...

# 备份名
dir=$1

# 保留天数
days=$2

# 备份文件列表
files=($*)
unset files[0]
unset files[0]

# 如果备份目录不存在则创建它
if [ ! -e /backup/$dir ]; then
  mkdir -p /backup/$dir
fi

# 备份
/usr/bin/zip -q -r /backup/$dir/${dir}_`date +\%Y\%m\%d`.zip ${files[@]}

# 清理旧的备份文件
days=$[$days+1]
ls -t /backup/$dir/${dir}_*.zip|tail -n +$days|xargs -r rm -f
